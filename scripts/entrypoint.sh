#!/bin/sh

set -e
echo "Calling Python manage.py"
python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate
echo "Completed Calling Python manage.py"

echo "Calling uwsgi"
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
echo "Completed Calling uwsgi"

